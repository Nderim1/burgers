angular.module('random').controller('AddCtrl', ['$scope', '$state', '$mdToast', 'RandomSrv', function($scope, $state, $mdToast, RandomSrv){

$scope.title = $state.current.data.title;
	$scope.save = function(burger) {
		console.log("Burger Saved " + burger.name);

		RandomSrv.save(burger)
		.then(function(){
			console.log('burger saved');
			 $state.go('home');
			 $mdToast.show($mdToast.simple().content("New burger saved successfuly!"));
		}).catch(function(err){
			console.log(err);
		});
	};

}])
