angular.module('random').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	 $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

 	$stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/random.template.html',
        controller: 'RandomCtrl'

      })
      .state('addBurger', {
        url: '/addburger',
        templateUrl: 'app/addBurger/addBurger.template.html',
        controller: 'AddCtrl',
				data: {
					title: 'Add new Burger'
				},
				onEnter: function(){console.warn('Siamo nello state addBurger');}
      })
			.state('update', {
        url: '/update/:id',
        templateUrl: 'app/addBurger/addBurger.template.html',
        controller: function($scope, $state, burger){
						  $scope.burger = burger;
							$scope.title = $state.current.data.title;
				},
				data: {
					title: 'Update'
				},
        resolve: {
           burger: function ($stateParams, RandomSrv) {
            return RandomSrv.get({id: $stateParams.id});
          }
        }
      })

});
