angular.module('random').controller('RandomCtrl', ['$scope', '$state', '$mdToast', '$timeout', 'RandomSrv', function($scope, $state, $mdToast, $timeout, RandomSrv, burger){

	//is trigered by button in the view and resolve the promise returned from the function in randomBurgers.controller.js (server-side)
	$scope.burger = burger;

	$scope.randomize = function () {
		RandomSrv.randomize()
			.then(function(burger){
				//$scope.showTable(burger);
				$scope.burgerId = burger._id;
				$scope.burgerName = burger.name;
				$scope.burgerDescription = burger.description;
				$scope.burgerPrice = burger.price;
				$scope.burgerImage = burger.image;


		}).catch(function(err){
				console.log(err);
			});
	};


$scope.popolate = function(burgerId){
	$state.transitionTo('update', {id: burgerId});
	console.log('clicked ' + burgerId);

}


$scope.showTable = function(burger){
	console.table([{name: burger.name, price: burger.price}]);
}


	$scope.remove = function(burgerId){

		console.log(burgerId);
		RandomSrv.remove({id: burgerId})
			.then(function(){
				$mdToast.show($mdToast.simple().content("Burger removed successfuly!"));
				console.log("Burger removed");
			})
			.then(function(){
				      $timeout(function(){
				      	 angular.element(randomize).triggerHandler('click');
				      }, 0)
			})
			.catch(function(err){
				console.log("ERROR: " + err);
			})
	}


}]);
