angular.module('random').factory('RandomSrv',  function($resource){
	//business logics
	var Burgers = $resource('/api/burgers/:action/:id',
		{
			action: '@action',
			id: '@id'
		}, {
		query: {
	        isArray: false,
	        method: 'GET',
	        params: { action: 'randomize' }
	    	}
	});



//triger server's QUERY() function
 var randomize = function (callback) {
    callback = callback || angular.noop;
    console.log('entra in service');
    return Burgers.query(function (burger) {
      return callback(burger);
    }, function (err) {
      return callback(err);
    }).$promise;
  };


//triger server's SAVE() function
var save = function (burger, callback){
	callback = callback || angular.noop;
	console.log("burger nel service");
	return Burgers.save(burger, function(){
		return callback();
	}, function(err){
		return callback(err);
	}).$promise;
};


//triger server's REMOVE() function
var remove = function(id, callback){
	callback = callback || angular.noop;

	return Burgers.remove(id, function(){
		return callback();
	}, function(err){
		return callback(err);
	}).$promise;
};


var get = function(params, callback){
	callback = callback || angular.noop;
	console.log('ID in service ' + params);
	return Burgers.get(params, function(burger){
		
		return callback(burger);
	}, function(err){
		return callback(err);
	}).$promise;
};

	//public API
	return {
		save: save,
		randomize: randomize,
		remove: remove,
		get: get
	};
})
