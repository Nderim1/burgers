var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//schema
var burgerSchema = new Schema({
    name: String,
    description: String, 
    price: Number,
    image: String
});

//methods


//middle
var Burger = mongoose.model('Burger', burgerSchema);

module.exports = Burger;