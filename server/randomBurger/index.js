var express = require('express');
var controller = require('./randomBurgers.controller.js')();
var router = express.Router();


router.get('/randomize', controller.query);
router.get('/:id', controller.get);
router.delete('/:id', controller.remove);
router.post('/', controller.save);
module.exports = router;
