  var Burger = require('./burgers.model');

module.exports = function () {
	//business logics



    //returns a random number
	var randomNr = function(burgerNr){
		var randomNr = Math.floor(Math.random() * burgerNr || 1);
		console.warn("Random number: " + randomNr);
		return randomNr;
	}


	//returns an array of objects containing burger's name, price, description and image url
	var query = function(req, res){

		Burger.find().exec()
			.then(function(burgers){
				var qta = burgers.length;
				console.warn("Burgers found in DB: " +qta);
				var randomBurger = burgers[randomNr(qta)];
		      return res.json(randomBurger);
	    	})
			.catch(function(err){
	      		res.status(500).send("ERROR: " + err);
	    	});
	}


var get = function(req, res){
  console.warn(req.params.id);
  Burger.findById({_id: req.params.id}).exec().then(function(burger){
    console.warn('Burger trovato: ' + burger);
    res.json(burger);
  }).catch(function(err){
    res.send('ERRORE: ' + err);
  });
}

	//save on the DB
  var save = function(req,res){
  	var burger = new Burger(req.body);
      console.warn(req.body);

  	var id = req.body._id || burger;
  	Burger.update({_id: id}, req.body, {upsert: true})
  	.then(
  		function(){
  			res.status(200).send("Data saved!");
  		}).catch(
  		function(err){
  			console.log(err);
  			res.status(500).send("Data not saved!");
  		}
  		);
  };




/*	var save = function(req, res){
		var burger = new Burger(req.body);




  burger.findOneAndUpdate({_id: req.body_id}, req.body, {upsert: true})
      .then(function(){
        return res.status(200).send("burger saved to DB");
      })
      .catch(function(err){
          console.error("Errore " + err);
      });

    burger.save(function(err, burger, numAffect){
			console.warn("ERROR: " + err);
			console.warn("Burger saved: " + burger);
			console.warn("Number of rows affected: " + numAffect);
		})
			.then(function(){
				return res.status(200).send("burger saved to DB");
			})
			.catch(function(err){
		  		console.error("Errore " + err);
			});
	}*/


	//remove from DB
	var remove = function(req, res){
		Burger.findByIdAndRemove(req.params.id).exec()
			.then(function() {
       			 res.status(200).send('Burger removed');
    		})
    		.catch(function(err){
    			res.status(500).send("ERROR: " + err);
    		});
	}



	//public API
	return{
    get: get,
		query: query,
		remove: remove,
		save: save
	};
};
