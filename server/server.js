var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var path = require('path');


// app.use(require('connect-livereload')({port: 7777}));

// connect to database
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

console.warn(process.env.MONGO_URI);
mongoose.connect(process.env.MONGO_URI, function () {
  console.info('Connection to the database was successfull');
});

// create a model
var Burger = require('./randomBurger/burgers.model');   //richiedo il modulo Burger

/*var myBurger = new Burger({
  name: 'NderimBurger',
  description: 'My huge burger',
  price: 5,
  image: 'path/to/file/img.jpg'
});


myBurger.save().then(function(){console.warn("burger saved")}).catch(function(err){
  console.error("Errore " + err);
});


console.error(myBurger);*/

//Burger.remove().exec().then(function(){console.warn("burger removed")}).catch();

// Setup server
var app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, '..', 'client')));


//my API
app.use('/api/burgers', require('./randomBurger'));


app.get('/:url(api|bower_components|assets)/*', function (req, res) {
  res.status(404).send( 'Resource not found');
});

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '..', 'client', 'index.html' ));
});

// Define server
var server = require('http').createServer(app);
// Start server
server.listen(8080, function () {
  console.log('Express server listening on %d', 8080);
});
